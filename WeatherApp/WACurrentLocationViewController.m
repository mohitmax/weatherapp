//
//  WAFirstViewController.m
//  WeatherApp
//
//  Created by demo on 7/23/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import "WACurrentLocationViewController.h"
#import "YOSSocial.h"
#import "NSString+SBJSON.h"
#import "SBJSON.h"
#import "WAConstants.h"

@interface WACurrentLocationViewController ()

@end

@implementation WACurrentLocationViewController
{
    BOOL updatingLocation;
    NSError *lastLocationError;
    NSString *key;
    NSString *secret;
    NSString *appId;
    int woeid;
    NSString *basicWeather;
    NSString *basicWeatherLocation;
    NSString *country;
}

@synthesize messageLabel;
@synthesize latitudeLabel;
@synthesize longitudeLabel;
@synthesize countryLabel;
@synthesize getButton;
@synthesize session;
@synthesize oauthResponse;
@synthesize yahooWeatherUrl;
@synthesize locationManager;
@synthesize location;
@synthesize basicWeatherInfoLabel;
@synthesize weatherLocationInfoLabel;


-(id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder])
    {
      
    }
    
    key = @"dj0yJmk9YWxZWk1tZlJFc3l5JmQ9WVdrOWJrMDFSV0ZuTlRBbWNHbzlPVGd6TlRjeU1qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTI1";
    secret = @"775030d35b3a1700329315f80a707e7f889652b5";
    appId = @"nM5Eag50";

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateLabels];
}

- (IBAction)getLocation:(id)sender
{
    locationManager = [[CLLocationManager alloc] init];
    [self startLocationManager];
    [self updateLabels]; 
}

- (void)createYahooSession
{
    self.session = [YOSSession sessionWithConsumerKey:key andConsumerSecret:secret andApplicationId:appId];
    
    [self sendRequests];
}

- (void)sendRequests
{
    YQLQueryRequest *request = [YQLQueryRequest requestWithSession:self.session];
    //NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select woeid from geo.placefinder where text=\"%f,%f\" and gflags=\"R\"",location.coordinate.latitude,location.coordinate.longitude];
    NSString *structuredProfileLocationQuery = [NSString stringWithFormat:YAHOO_WOEID_QUERY,location.coordinate.latitude,location.coordinate.longitude];
    [request query:structuredProfileLocationQuery withDelegate:self];
}

- (void)requestDidFinishLoading:(YOSResponseData *)data
{
    NSDictionary *rspData = [data.responseText JSONValue];
    NSDictionary *queryData = [rspData objectForKey:@"query"];
    NSDictionary *results = [queryData objectForKey:@"results"];
    NSDictionary *exampleWoeid = [results objectForKey:@"Result"];
        
    NSNumber *sampleWoeid = [exampleWoeid objectForKey:@"woeid"];
    
    [self fetchWeather:sampleWoeid];
}

- (void)fetchWeather:(NSNumber *)woeidNumber
{
    //NSString *urlString = [NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=%@", woeidNumber];
    NSString *urlString = [NSString stringWithFormat:YAHOO_API_URL, woeidNumber];
    yahooWeatherUrl = [NSURL URLWithString:urlString];
    
    [self parseWeather];
}

#pragma mark - Parsing the XML from yahoo! weatherrss feed.
- (void)parseWeather
{
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:yahooWeatherUrl];
    [parser setDelegate:self];
    [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:WEATHER_LOCATION])
    {
        self.locationCity = [attributeDict objectForKey:@"city"];
        self.locationRegion = [attributeDict objectForKey:@"region"];
        self.locationCountry = [attributeDict objectForKey:@"country"];
    }
    
    if([elementName isEqualToString:WEATHER_UNIT])
    {
        self.weatherTempUnit = [attributeDict objectForKey:@"temperature"];
    }
    
    if([elementName isEqualToString:WEATHER_HIMIDITY])
    {
        self.weatherHumidity = [attributeDict objectForKey:@"humidity"];
    }
    
    if([elementName isEqualToString:WEATHER_BASIC])
    {
        self.weatherTempValue = [attributeDict objectForKey:@"temp"];
        self.weathertext = [attributeDict objectForKey:@"text"];
        self.weatherDate = [attributeDict objectForKey:@"date"];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:@"rss"])
    {
        basicWeather = [NSString stringWithFormat:@"%@, %@ %@",self.weathertext, self.weatherTempValue, self.weatherTempUnit];
        basicWeatherLocation = [NSString stringWithFormat:@"%@ %@", self.locationCity, self.locationRegion];
        country = [NSString stringWithFormat:@"%@", self.locationCountry];
        [self updateWeatherLabels];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if(error.code == kCLErrorLocationUnknown)
    {
        return;
    }
    
    [self stopLocationManager];
    lastLocationError = error;
    
    [self updateLabels];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if ([newLocation.timestamp timeIntervalSinceNow] < -5.0)
    {
       return;
    }
    
    if (newLocation.horizontalAccuracy < 0)
    {
        return;
    }

    lastLocationError = nil;
    location = newLocation;
    [self updateLabels];
        
    if (newLocation.horizontalAccuracy <= locationManager.desiredAccuracy)
    {
        [self stopLocationManager];
    }
}

- (void)updateLabels
{
    if(location != nil)
    {
        self.messageLabel.text = @"Weather";
        self.latitudeLabel.text = [NSString stringWithFormat:@"%0.8f",location.coordinate.latitude];
        self.longitudeLabel.text = [NSString stringWithFormat:@"%0.8f",location.coordinate.longitude];
        
        [self createYahooSession];
    }
    else
    {
        self.latitudeLabel.text = @"";
        self.longitudeLabel.text = @"";
        self.countryLabel.text = @"";
        self.basicWeatherInfoLabel.text = @"";
        self.weatherLocationInfoLabel.text = @"";
        self.countryLabel.text = @"";
        
        NSString *statusMessage;
        if (lastLocationError != nil)
        {
            if ([lastLocationError.domain isEqualToString:kCLErrorDomain] && lastLocationError.code == kCLErrorDenied)
            {
                statusMessage = @"Location Services Disabled";
            }
            else
            {
                statusMessage = @"Error Getting Location";
            }
        }
        else if (![CLLocationManager locationServicesEnabled])
        {
            statusMessage = @"Location Services Disabled";
        }
        else if (updatingLocation)
        {
            statusMessage = @"Searching...";
        }
        else
        {
            statusMessage = @"Press the Button to Start";
        }
        
        self.messageLabel.text = statusMessage;
    }
}

- (void)updateWeatherLabels
{
    if(location != nil)
    {
        self.basicWeatherInfoLabel.text = basicWeather;
        self.weatherLocationInfoLabel.text = basicWeatherLocation;
        self.countryLabel.text = country;
    }
    else
    {
        self.basicWeatherInfoLabel.text = @"";
        self.weatherLocationInfoLabel.text = @"";
        self.countryLabel.text = @"";
    }
}

- (void)startLocationManager
{
    location = nil;
    
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [locationManager startUpdatingLocation];
        updatingLocation = YES;
    }
}

- (void) stopLocationManager
{
    [locationManager stopUpdatingLocation];
    locationManager.delegate = self;
    updatingLocation = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) dealloc
{
    [location release];
    [locationManager release];
    
    [super dealloc];
}
@end
