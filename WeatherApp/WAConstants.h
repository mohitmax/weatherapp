//
//  WAConstants.h
//  WeatherApp
//
//  Created by demo on 7/26/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <Foundation/Foundation.h>

#define YAHOO_WOEID_QUERY @"select woeid from geo.placefinder where text=\"%f,%f\" and gflags=\"R\""
#define YAHOO_API_URL @"http://weather.yahooapis.com/forecastrss?w=%@"

#define WEATHER_LOCATION @"yweather:location"
#define WEATHER_UNIT @"yweather:units"
#define WEATHER_HIMIDITY @"yweather:atmosphere"
#define WEATHER_BASIC @"yweather:condition"


@interface WAConstants : NSObject

@end
