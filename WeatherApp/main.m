//
//  main.m
//  WeatherApp
//
//  Created by demo on 7/23/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WAAppDelegate class]));
    }
}
