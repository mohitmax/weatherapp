//
//  WAFirstViewController.h
//  WeatherApp
//
//  Created by demo on 7/23/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "YOSSocial.h"

@interface WACurrentLocationViewController : UIViewController<CLLocationManagerDelegate, NSXMLParserDelegate>
{
    YOSSession					*session;
	NSMutableDictionary			*oauthResponse;
}

@property (nonatomic, strong) IBOutlet UILabel *messageLabel;
@property (nonatomic, strong) IBOutlet UILabel *latitudeLabel;
@property (nonatomic, strong) IBOutlet UILabel *longitudeLabel;
@property (nonatomic, strong) IBOutlet UILabel *countryLabel;
@property (nonatomic, strong) IBOutlet UILabel *basicWeatherInfoLabel;
@property (nonatomic, strong) IBOutlet UILabel *weatherLocationInfoLabel;
@property (nonatomic, strong) IBOutlet UIButton *getButton;
@property (nonatomic, strong) NSURL *yahooWeatherUrl;

@property (nonatomic, readwrite, retain) YOSSession *session;
@property (nonatomic, readwrite, retain) NSMutableDictionary *oauthResponse;

//properties to store the weather values
@property (nonatomic, strong) NSString *weathertext;
@property (nonatomic, strong) NSNumber *weatherTempValue;
@property (nonatomic, strong) NSString *weatherDate;
@property (nonatomic, strong) NSString *locationCity;
@property (nonatomic, strong) NSString *locationRegion;
@property (nonatomic, strong) NSString *locationCountry;
@property (nonatomic, strong) NSString *weatherTempUnit;
@property (nonatomic, strong) NSNumber *weatherHumidity;

//trying this shit out
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *location;



- (void)createYahooSession;
- (IBAction)getLocation:(id)sender;

@end
