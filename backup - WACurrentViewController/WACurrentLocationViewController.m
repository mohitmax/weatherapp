//
//  WAFirstViewController.m
//  WeatherApp
//
//  Created by demo on 7/23/13.
//  Copyright (c) 2013 Mohit Sadhu. All rights reserved.
//

#import "WACurrentLocationViewController.h"
#import "YOSSocial.h"
#import "NSString+SBJSON.h"
#import "SBJSON.h"


@interface WACurrentLocationViewController ()

@end

@implementation WACurrentLocationViewController
{
    //CLLocationManager *locationManager;
    //CLLocation *location;
    BOOL updatingLocation;
    NSError *lastLocationError;
    NSString *key;
    NSString *secret;
    NSString *appId;
    int woeid;
    NSString *basicWeather;
    NSString *basicWeatherLocation;
}

@synthesize messageLabel;
@synthesize latitudeLabel;
@synthesize longitudeLabel;
@synthesize addressLabel;
@synthesize getButton;
@synthesize session;
@synthesize oauthResponse;
@synthesize yahooWeatherUrl;

//trying this shit out as it keeps getting deallocated
@synthesize locationManager;
@synthesize location;


-(id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super initWithCoder:aDecoder])
    {
       // locationManager = [[CLLocationManager alloc] init];
    }
    
    key = @"dj0yJmk9YWxZWk1tZlJFc3l5JmQ9WVdrOWJrMDFSV0ZuTlRBbWNHbzlPVGd6TlRjeU1qWXkmcz1jb25zdW1lcnNlY3JldCZ4PTI1";
    secret = @"775030d35b3a1700329315f80a707e7f889652b5";
    appId = @"nM5Eag50";

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateLabels];
}

- (IBAction)getLocation:(id)sender
{
    NSLog(@"location retain count %d", [location retainCount]);
    location = nil;
    locationManager = [[CLLocationManager alloc] init];
    
    //[location retain];
    
    NSLog(@"location retain count %d", [location retainCount]);
    [self startLocationManager];
    NSLog(@"location retain count %d", [location retainCount]);
    [self updateLabels];
    NSLog(@"location retain count %d", [location retainCount]);
    //[self createYahooSession]; //testing. moving it to updateLabels
}

- (void)createYahooSession
{
    NSLog(@"inside createyahoo session");
    self.session = [YOSSession sessionWithConsumerKey:key andConsumerSecret:secret andApplicationId:appId];
    
    [self sendRequests];
}

- (void)sendRequests
{
    NSLog(@"inside sendRequest");
    NSLog(@"latitude %f", location.coordinate.latitude);
    NSLog(@"latitude %f", location.coordinate.longitude);
    
    YQLQueryRequest *request = [YQLQueryRequest requestWithSession:self.session];
    NSString *structuredProfileLocationQuery = [NSString stringWithFormat:@"select woeid from geo.placefinder where text=\"%f,%f\" and gflags=\"R\"",location.coordinate.latitude,location.coordinate.longitude];
    [request query:structuredProfileLocationQuery withDelegate:self];
}

- (void)requestDidFinishLoading:(YOSResponseData *)data
{
    NSLog(@"inside requestDidFinishLoading method");
    NSDictionary *rspData = [data.responseText JSONValue];
    NSDictionary *queryData = [rspData objectForKey:@"query"];
    NSDictionary *results = [queryData objectForKey:@"results"];
    NSDictionary *exampleWoeid = [results objectForKey:@"Result"];
    NSLog(@"%@",[exampleWoeid valueForKey:@"woeid"]);
    
    NSNumber *sampleWoeid = [exampleWoeid objectForKey:@"woeid"];
    
    NSLog(@"results dictionary : %@", [results description]);
    NSLog(@"woeid : %@", sampleWoeid);
    
    [self fetchWeather:sampleWoeid];
}

- (void)fetchWeather:(NSNumber *)woeidNumber
{
    NSString *urlString = [NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=%@", woeidNumber];
    yahooWeatherUrl = [NSURL URLWithString:urlString];
    
    NSString *xmlString = [[NSString alloc] initWithContentsOfURL:yahooWeatherUrl encoding:NSASCIIStringEncoding error:nil];
    NSLog(@"%@", xmlString);
    
    [self parseWeather];
}

#pragma mark - Parsing the XML from yahoo! weatherrss feed.
- (void)parseWeather
{
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:yahooWeatherUrl];
    NSLog(@"nsxml : %@", parser);
    [parser setDelegate:self];
    [parser parse];
    //BOOL result = [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    NSLog(@"Did start element %@", elementName);

    if([elementName isEqualToString:@"yweather:location"])
    {
        self.locationCity = [attributeDict objectForKey:@"city"];
        self.locationRegion = [attributeDict objectForKey:@"region"];
        self.locationCountry = [attributeDict objectForKey:@"country"];
    }
    
    if([elementName isEqualToString:@"yweather:units"])
    {
        self.weatherTempUnit = [attributeDict objectForKey:@"temperature"];
    }
    
    if([elementName isEqualToString:@"yweather:atmosphere"])
    {
        self.weatherHumidity = [attributeDict objectForKey:@"humidity"];
    }
    
    if([elementName isEqualToString:@"yweather:condition"])
    {
        NSLog(@"Did start elementtemp value %@", [attributeDict objectForKey:@"temp"]);
        self.weatherTempValue = [attributeDict objectForKey:@"temp"];
        self.weathertext = [attributeDict objectForKey:@"text"];
        self.weatherDate = [attributeDict objectForKey:@"date"];
    }
    
    NSLog(@"Weather Condition: \n %@, %@, %@ ,\n%@, %@, %@, \n Humidity = %@", self.weathertext, self.weatherTempValue, self.weatherTempUnit, self.locationCity, self.locationRegion, self.locationCountry, self.weatherHumidity);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSLog(@"Did End Element %@", elementName);
    //NSLog(@"location retain count parser DidEndElement %d", [location retainCount]);
    if([elementName isEqualToString:@"rss"])
    {
        NSLog(@"*******    this is the end    **********");
        basicWeather = [NSString stringWithFormat:@"%@, %@ %@",self.weathertext, self.weatherTempValue, self.weatherTempUnit];
        basicWeatherLocation = [NSString stringWithFormat:@"%@, %@", self.locationCity, self.locationRegion];
        [self updateWeatherLabels];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    NSLog(@"Found characters");
    NSString *tagName = @"description";
    
    if([tagName isEqualToString:@"yweather:units"])
    {
        NSLog(@" hellloooooo  temperature unit is : %@", string);
    }
    
    if([tagName isEqualToString:@"description"])
    {
        NSLog(@"Description: %@", string);
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError %@", error);
    
    if(error.code == kCLErrorLocationUnknown)
    {
        return;
    }
    
    [self stopLocationManager];
    lastLocationError = error;
    
    [self updateLabels];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"location retain count didupdatelocation %d", [location retainCount]);
    NSLog(@"didUpdateFromLocation %@", oldLocation);
    NSLog(@"didUpdateToLocation %@", newLocation);
    if ([newLocation.timestamp timeIntervalSinceNow] < -5.0)
    {
        NSLog(@"newlocation timestamp interval error");
       return;
    }
    
    if (newLocation.horizontalAccuracy < 0)
    {
        NSLog(@"newlocation horizontal accuracy error");
        return;
    }
    
    if (location == nil || location.horizontalAccuracy > newLocation.horizontalAccuracy)
    {
        
        lastLocationError = nil;
        location = newLocation;
        [self updateLabels];
        
        if (newLocation.horizontalAccuracy <= locationManager.desiredAccuracy)
        {
            NSLog(@"*** We're done!");
            [self stopLocationManager];
        }
    }
}

- (void)updateLabels
{
    NSLog(@"location retain count update labels %d", [location retainCount]);
    NSLog(@"locationmanager retain count update labels %d", [locationManager retainCount]);
    NSLog(@"%f", location.coordinate.latitude);
    if(location != nil)
    {
        NSLog(@"location retain count inside updatelabels %d", [location retainCount]);
        NSLog(@"lat : %0.4f", location.coordinate.latitude);
        NSLog(@"lat : %0.4f", location.coordinate.longitude);
        
        self.messageLabel.text = @"GPS Coordinates";
        self.latitudeLabel.text = [NSString stringWithFormat:@"%0.8f",location.coordinate.latitude];
        self.longitudeLabel.text = [NSString stringWithFormat:@"%0.8f",location.coordinate.longitude];
        //self.tagButton.hidden = NO;
        
        [self createYahooSession]; //testing
    }
    else
    {
        self.latitudeLabel.text = @"";
        self.longitudeLabel.text = @"";
        self.addressLabel.text = @"";
        self.basicWeatherInfoLabel.text = @"";
        self.weatherLocationInfoLabel.text = @"";
        //self.tagButton.hidden = YES;
        
        NSString *statusMessage;
        if (lastLocationError != nil)
        {
            if ([lastLocationError.domain isEqualToString:kCLErrorDomain] && lastLocationError.code == kCLErrorDenied)
            {
                statusMessage = @"Location Services Disabled";
            }
            else
            {
                statusMessage = @"Error Getting Location";
            }
        }
        else if (![CLLocationManager locationServicesEnabled])
        {
            statusMessage = @"Location Services Disabled";
        }
        else if (updatingLocation)
        {
            statusMessage = @"Searching...";
        }
        else
        {
            statusMessage = @"Press the Button to Start";
        }
        
        self.messageLabel.text = statusMessage;
    }
}

- (void)updateWeatherLabels
{
    NSLog(@"location retain count updateWeatherLabels %d", [location retainCount]);
    if(location != nil)
    {
        self.basicWeatherInfoLabel.text = basicWeather;
        self.weatherLocationInfoLabel.text = basicWeatherLocation;

    }
    else
    {
        self.basicWeatherInfoLabel.text = @"";
        self.weatherLocationInfoLabel.text = @"";

    }
    
    //trying some new thing by calling stopLocationManager from here as well as this is the last method to be called
    //[self stopLocationManager];
}

- (void)startLocationManager
{
    location = nil;
    
    NSLog(@"location retain count startlocationmanager %d", [location retainCount]);
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [locationManager startUpdatingLocation];
        updatingLocation = YES;
    }
    NSLog(@"location retain count startlocationmanager2 %d", [location retainCount]);
}

- (void) stopLocationManager
{
    NSLog(@"location retain count STOPlocationmanager %d", [location retainCount]);
    if(updatingLocation)
    {
        [locationManager stopUpdatingLocation];
        locationManager.delegate = self;
        updatingLocation = NO;
    }
    NSLog(@"location retain count STOPlocationmanager2 %d", [location retainCount]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) dealloc
{
    [location release];
    [locationManager release];
    NSLog(@"location retain count dealloc %d", [location retainCount]);
    
    [super dealloc];
}
@end
